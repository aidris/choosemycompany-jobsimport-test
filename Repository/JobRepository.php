<?php

class JobRepository
{
    private $connection;
    
    /**
     * 
     * @param DatabaseConnection $dataBaseConnection
     */
    public function __construct($dataBaseConnection)
    {
        $this->connection = $dataBaseConnection->getConnection();
    }
    
    /**
     * 
     * @param array $data
     * @return boolean
     * @todo: use PDO for insert
     */
    public function save($data)
    {
        $result = $this->connection->exec('INSERT INTO job (reference, title, description, url, company_name, publication) VALUES ('
            . '\'' . addslashes($data['reference']) . '\', '
            . '\'' . addslashes($data['title']) . '\', '
            . '\'' . addslashes($data['description']) . '\', '
            . '\'' . addslashes($data['url']) . '\', '
            . '\'' . addslashes($data['company_name']) . '\', '
            . '\'' . addslashes($data['publication']) . '\')'
         );
        
        return $result == 1;
    }
    
    /**
     * 
     * @return array
     */
    public function getJobs()
    {
        return $this->connection->query('SELECT id, reference, title, description, url, company_name, publication FROM job')->fetchAll(PDO::FETCH_ASSOC);
    }
}
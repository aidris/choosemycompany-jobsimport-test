<?php

/**
    CREATE DATABASE cmc_test;
    USE cmc_test;
    CREATE TABLE job (
        id int NOT NULL auto_increment,
        reference varchar(255),
        title varchar(255),
        description TEXT,
        url varchar(255),
        company_name varchar(255),
        publication DATETIME,
        PRIMARY KEY(id)
    );
*/

require_once __DIR__ . '/Database/DatabaseConnection.php';
require_once __DIR__ . '/Repository/JobRepository.php';
require_once __DIR__ . '/JobsImporter.php';

define('SQL_HOST', 'localhost');
define('SQL_USER', 'root');
define('SQL_PWD', 'root');
define('SQL_DB', 'cmc_test');

if (! isset($argv[1])) {
    echo sprintf("You have to specify company to import file\n");
    exit;
}

if ($argv[1] != 'regionsjob' && $argv[1] != 'cadremploi') {
    echo sprintf("Invalid company to import file\n");
    echo sprintf("Valid values : regionsjob or cadremploi\n");
    exit;
}

echo sprintf("Starting...\n");

// Connect to databse
$dataBaseConnection = new DatabaseConnection(SQL_HOST, SQL_USER, SQL_PWD, SQL_DB);

// Clean Jobs
$dataBaseConnection->getConnection()->exec('DELETE FROM job');

$jobRepository = new JobRepository($dataBaseConnection);

/* import jobs from partner */
$jobsImporter = new JobsImporter($jobRepository);
$count = $jobsImporter->importJobs($argv[1]);
echo sprintf("> %d jobs imported.\n", $count);

/* list jobs */
foreach ($jobRepository->getJobs() as $job) {
    echo sprintf(" %d: %s - %s - %s\n", $job['id'], $job['reference'], $job['title'], $job['publication']);
}

echo sprintf("Done.\n");

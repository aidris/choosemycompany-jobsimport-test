<?php

class JobsImporter
{
    private $jobRepository;
    
    /**
     * 
     * @param JobRepository $jobRepository
     */
    public function __construct($jobRepository)
    {
        $this->jobRepository = $jobRepository;
    }

    /**
     * 
     * @param string $partner
     * 
     */
    public function importJobs($partner)
    {
        $configuration = $this->getConfiguration($partner);
        
        if (empty($configuration) || ! isset($configuration['fields']) || ! isset($configuration['file'])) {
            return 0;
        }
        
        $fields = $configuration['fields'];
        $file = $configuration['file'];

        $xml = simplexml_load_file($file);
        
        $count = 0;
        foreach ($xml as $item) {
            // Create job array values
            $job = [];
            foreach ($fields as $key => $field) {
                if ($field == 'publication') {
                    $dateTime = new \DateTime($item->$key);
                    $value = $dateTime->format('Y-m-d H:i:s');
                } else {
                    $value = $item->$key;
                }
                $job[$field] = $value;
            }
            
            if (!empty($job)) {
                // save Job
                $result = $this->jobRepository->save($job);
                if ($result) {
                    $count++;
                }
            }
        }
        
        return $count;
    }
    
    /**
     * 
     * @param string $partner
     * @return array
     * @todo: refuse file configuration to import partner parameters
     * 
     * build parnet import file configuration
     */
    private function getConfiguration($partner)
    {
        $fields = [];
        switch($partner) {
            case 'regionsjob':
                $fields = [
                    'ref' => 'reference',
                    'title' => 'title',
                    'description' => 'description',
                    'url' => 'url',
                    'company' => 'company_name',
                    'pubDate' => 'publication'
                ];
                return [
                    'fields' => $fields,
                    'file' => 'regionsjob.xml'
                ];
            case 'cadremploi':
                $fields = [
                    'reference' => 'reference',
                    'title' => 'title',
                    'description' => 'description',
                    'link' => 'url',
                    'companyname' => 'company_name',
                    'publisheddate' => 'publication'
                ];
                return [
                    'fields' => $fields,
                    'file' => 'cadremploi.xml'
                ];
                break;
            default:
                return null;
        }
    }
}

<?php

class DatabaseConnection
{
    private $connection;
    
    /**
     * 
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $databaseName
     */
    public function __construct($host, $username, $password, $databaseName)
    {
        /* connect to DB */
        try {
            $this->connection = new PDO('mysql:host=' . $host . ';dbname=' . $databaseName, $username, $password);
        } catch (Exception $e) {
            die('Error : ' . $e->getMessage() . "\n");
        }
    }
    
    public function getConnection()
    {
        return $this->connection;
    }
}